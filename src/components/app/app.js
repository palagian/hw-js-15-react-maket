import React from "react"
import Items from "../items/items"
import Pic from "../img/img"
import Content from "../content/content"
import Button from "../button/button"
import Footer from "../footer/footer"
import "./app.css"

const Maket = () => {
    return (
        <>
            <header>
                <ul>
                    <Items></Items>
                </ul>
            </header>
            <div className="banner">
                <Pic></Pic>
            </div>
            <div>
                <Content></Content>
            </div>
            <div>
                <Button></Button>
            </div>
            <div>
                <Footer></Footer>
            </div>
        </>
    )
}

export default Maket