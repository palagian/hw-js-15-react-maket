import React from "react"
import pic from "../img/top-img@1X.png"

const Pic = () => {
    return (
        <img src={pic} alt="pic"></img>
    )
}

export default Pic
