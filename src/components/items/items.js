import React from "react"
import menu from "../../menu"
import "./items.css"

const Items = (() => {
    return (
        <>
            {
                menu.map((item, i) => {
                    return <li className="menu" key={i*12} >{item}</li>
                })
            }
        </>
    )
})

export default Items